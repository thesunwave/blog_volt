Rails.application.routes.draw do

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :users, only: [:create, :show]
      resources :posts, except: [:edit, :update, :new]
    end
  end

end
