class Cranky::Factory

  def post
    p       = Post.new
    p.author  = options[:author] || Factory.create(:user)
    p.body = options[:body] || Faker::Hipster.paragraph
    p.title  = options[:title] || Faker::Hipster.sentence
    p
  end

end
