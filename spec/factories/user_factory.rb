class Cranky::Factory

  def user
    u       = User.new
    u.nickname  = options[:nickname] || Faker::Name.name
    u.email = options[:email] || Faker::Internet.email
    u.password  = options[:password] || Faker::Internet.password
    u
  end

end
