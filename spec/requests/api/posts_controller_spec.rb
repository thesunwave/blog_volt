require 'rails_helper'

describe 'Api::PostsController', type: :request do
  describe 'POST /api/v1/posts' do
    let(:user) { Factory.create(:user) }
    let(:user_post) { Factory.build(:post, author: user)}

    context 'authorized user' do
      let(:publish_date) { '2016-10-31' }

      it 'creates a post with valid params' do
        post api_v1_posts_path(token: user.api_key, title: user_post.title, body: user_post.body, published_at: publish_date)
        expect(response.status).to eq(200)

        json = JSON.parse(response.body)['post']
        expect(json['title']).to eq(user_post.title)
        expect(json['body']).to eq(user_post.body)
        expect(json['author']['nickname']).to eq(user.nickname)

        expect(user.posts.count).to eq(1)
      end

      it 'creates a post with valid params without published date' do
        post api_v1_posts_path(token: user.api_key, title: user_post.title, body: user_post.body)
        expect(response.status).to eq(200)

        json = JSON.parse(response.body)['post']
        expect(json['title']).to eq(user_post.title)
        expect(json['body']).to eq(user_post.body)
        expect(json['published_at']).to match(/#{Date.current}/)
        expect(json['author']['nickname']).to eq(user.nickname)

        expect(user.posts.count).to eq(1)
      end
    end

    context 'unauthorized user' do
      it 'returns an error when creating the post' do
        post api_v1_posts_path(user_post)
        expect(response.status).to eq(401)
      end
    end
  end
end
