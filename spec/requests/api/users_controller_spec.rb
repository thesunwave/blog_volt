require 'rails_helper'

describe 'Api::UsersController', type: :request do
  describe 'POST /api/v1/users' do
    context 'given the unique item' do
      let(:user) { Factory.build(:user) }

      it 'create user and return token' do
        post api_v1_users_path(nickname: user.nickname, email: user.email, password: user.password)
        expect(JSON.parse(response.body)['token']).not_to be_empty
      end

      it 'create user with invalid data' do
        post api_v1_users_path(nickname: user.nickname, email: user.email)
        expect(JSON.parse(response.body)['error']).not_to be_empty
        expect(JSON.parse(response.body)['token']).to be_nil
      end
    end
  end

  describe 'GET /api/v1/users/:id' do
    context 'gets current user as authorized user' do
      let(:user) { Factory.create(:user) }
      it 'returns current user' do
        get api_v1_user_path(id: user.id, token: user.api_key)
        expect(JSON.parse(response.body)['user']).to have_key('nickname')
        expect(JSON.parse(response.body)['user']).to have_key('id')
        expect(JSON.parse(response.body)['user']).to have_key('email')
      end
    end
    context 'gets current user as unauthorized user' do
      let(:user) { Factory.create(:user) }
      it 'returns authorized error' do
        get api_v1_user_path(id: user.id)
        expect(response.status).to eq(401)
      end
    end
  end
end
