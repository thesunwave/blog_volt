require 'rails_helper'

RSpec.describe Post, type: :model do
  let(:user) { Factory.create(:user) }
  let(:post) { Factory.create(:post, author: user)}

  it 'be valid' do
    expect(post.author).to eq user
  end
end
