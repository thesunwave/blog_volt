require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { Factory.build(:user) }

  it 'be valid' do
    expect(user.save!).to be_truthy
    expect(user.api_key).not_to be_empty
  end
end
