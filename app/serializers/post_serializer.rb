class PostSerializer < ActiveModel::Serializer
  attributes :id, :title, :body, :published_at

  has_one :author, serializer: UserPreviewSerializer
end
