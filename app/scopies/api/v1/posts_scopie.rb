module Api::V1
  class PostsScopie < ScopieRails::Base

    has_scope :page, default: 1
    has_scope :per, default: 5
    has_scope :ordered

  end
end
