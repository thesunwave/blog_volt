class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  def authenticate_token
    if params[:token].blank?
      render_unauthorized
    else
      @current_user ||= User.find_by_api_key(params[:token])
      render_unauthorized unless current_user
    end
  end
  alias_method :authenticate, :authenticate_token

  def current_user
    @current_user
  end

  private

  def render_unauthorized
    render json: { error: 'Bad credentials' }, status: :unauthorized and return
  end

end
