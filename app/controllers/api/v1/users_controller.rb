module Api::V1
  class UsersController < ApplicationController

    before_action :authenticate, only: :show

    def create
      user = User.new(params.permit(:nickname, :email, :password))
      if user.save
        render json: { token: user.api_key }
      else
        render json: { error: user.errors }
      end
    end

    def show
      user = User.find(params[:id])
      render json: user
    end

  end
end
