module Api::V1
  class PostsController < ApplicationController
    include ScopieRails::Controller

    before_action :authenticate, only: %i(create update destroy)

    def create
      post = Post.new(params.permit(:title, :body, :published_at))
      post.author = current_user
      post.published_at = params.fetch(:published_at, DateTime.current)

      if post.save
        render json: post
      else
        render json: { errors: post.errors }
      end
    end

    def index
      render json: apply_scopes(Post.ordered).all
    end

    def show
      post = Post.find(params[:id])
      render json: post
    end

  end
end
