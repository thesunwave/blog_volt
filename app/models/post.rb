class Post < ApplicationRecord
  belongs_to :author, class_name: User, foreign_key: 'user_id'

  validates :title, :body, presence: true

  scope :ordered, -> { order(published_at: :desc) }

  acts_as_commentable
end
